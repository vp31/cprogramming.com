#include<iostream>
#include<ios> //used to get stream size
#include<limits> //used to get numeric limits

int main()
{
    std::string name;
    std::cout << "Enter std::string name: ";
    std::cin >> name;
    std::cout << "std::string name: " << name << std::endl;
    //clear buffer before taking new line
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), 'n');
    std::cout << "Enter std::getline name: ";

    std::getline(std::cin, name, '\n');
    std::cout << "std::getline name: " << name << std::endl;

    std::cin.ignore();
    std::cin.get();

    return 0;
}
