#include <iostream>

using namespace std;

void playgame ()
{
    cout << "playgame" << endl;
}

void loadgame ()
{
    cout << "loadgame" << endl;
}

void playmultiplayer ()
{
    cout << "playmultiplayer" << endl;
}


enum RainbowColor {
RC_RED, RC_ORANGE, RC_YELLOW, RC_GREEN, RC_BLUE, RC_INDIGO, RC_VIOLET
};

int main ()
{

	int input;

	cout << "1. Play game\n";
	cout << "2. Load game\n";
	cout << "3. Play multiplayer\n";
	cout << "4. Exit\n";
	cout << "Selection: ";
	cin >> input;

	RainbowColor obj = static_cast<RainbowColor>(input);

	switch ( obj )
	{
	case RC_RED:            // Note the colon after each case, not a semicolon
		playgame();
		break;
	case RC_GREEN:
		loadgame();
		break;
	case RC_INDIGO:
		playmultiplayer();
		break;
	case 4:
		cout << "Thank you for playing!\n";
		break;
	default:            // Note the colon for default, not a semicolon
		cout << "Error, bad input, quitting\n";
		break;
	}
}
